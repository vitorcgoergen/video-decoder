module MEDIAN#(parameter WIDTH = 8) (input [WIDTH-1:0] DI,
                               input DSI,
                               input nRST,
                               input CLK,
                               output [WIDTH-1:0] DO,
                               output DSO);



logic [5:0] counter;             // Pour savoir le clk
logic myBYP,PreDSO;              //Signal interne BYP et un PreDSO
wire [WIDTH-1:0] preOutpt;       // Signal interne PreOutpt
//Definition des états
enum logic[1:0] {INIT, S0, S1} state;

MED myMED (.DI(DI),.DSI(DSI),.BYP(myBYP),.CLK(CLK),.DO(preOutpt));

//Processus de clock/reset pour le progression des états
always_ff @(posedge CLK, negedge nRST) begin
if (nRST == 0) begin
        state <= INIT;
end
else
    case (state)                                      // Si on est a INIT et le counter est < 9, on est en train de recevoir les pixels
        INIT: if(counter == 9) begin                  // Donc on reste a INIT. Si counter arrive a 9 on a tout recu et on doit commencer à calculer.
                state <= S0;
              end
              else if (counter < 9) begin
                state <= INIT;
              end
        S0 : if (counter > 38) begin                 // Si le counter arrive a 38 on a fini le calcul
                state <= S1;
            end
            else state <= S0;
        S1 : state <= INIT;                          // On arrive a cet état et retourne immediatement
        default : state <= INIT;
    endcase
end

assign DSO = PreDSO;                                // assignments immediates
assign DO = preOutpt;

always_ff @(posedge CLK,negedge nRST) begin
if (nRST == 0) begin
        counter <= 0;
        myBYP <= 1;
        PreDSO <= 0;
end
begin
    if (state == S0) begin                                        //on change BYP en accordance avec l'algorithme
        if (counter < 7) begin
            myBYP <= 0;
        end
        else if (counter == 7) begin
            myBYP <= 1;
        end
        else if (counter <= 14) begin
            myBYP <= 0;
        end
        else if (counter <= 16) begin
            myBYP <= 1;
        end
        else if (counter <= 22) begin
            myBYP <= 0;
        end
        else if (counter <= 25) begin
            myBYP <= 1;
        end
        else if (counter <= 30) begin
            myBYP <= 0;
        end
        else if (counter <= 34) begin
            myBYP <= 1;
        end
        else if (counter <= 38) begin
            myBYP <= 0;
        end
        else if (counter > 38) begin
            PreDSO <= 1;
        end
        counter <= counter + 1;
    end
    else if (state == S1) begin
        counter <= 1;
        myBYP <= 0;
        PreDSO <= 0;
    end
    else if (state == INIT) begin                              // Si on est à INIT et le counter < 9 et le DSI est 1 on doit recevoir les pixels
        if(counter < 9) begin
            if(DSI == 1) begin
                myBYP <= 1;
                counter <= counter + 1;
            end
        end
        else begin                                           // Si c'est pas le cas on vien d'arriver a cet état et on doit donc recommencer
            counter <= 0;
            PreDSO <= 0;
            myBYP <= 0;
        end
    end
    if (DSI == 1 && PreDSO == 1) begin                      //On doit remettre le DSO à zero
        PreDSO <= 0;
    end
end
end
endmodule