module MUX#(parameter WIDTH = 8) (input [WIDTH-1:0] A,
                                  input [WIDTH-1:0] B,
                                  input sel,
                                  output [WIDTH-1:0] Ot);

assign Ot = (sel == 0)? A:B;

endmodule
