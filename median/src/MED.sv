module MED#(parameter WIDTH = 8, parameter SIZE = 9) (input [WIDTH-1:0] DI,
                                                     input DSI,
                                                     input BYP,
                                                     input CLK,
                                                     output [WIDTH-1:0] DO);

logic [WIDTH-1:0] redwire,bluewire,RWire,max_circ,Wire8;
logic [WIDTH-1:0] R[SIZE-2:0],green;

MCE #(.WIDTH(WIDTH)) myMCE (.A(green),.B(bluewire),.MAX(max_circ),.MIN(redwire));
MUX #(.WIDTH(WIDTH)) myMUX1 (.A(redwire),.B(DI),.sel(DSI),.Ot(RWire));
MUX #(.WIDTH(WIDTH)) myMUX2 (.A(max_circ),.B(bluewire),.sel(BYP),.Ot(Wire8));

always_ff @(posedge CLK)
begin
    int i;
    for(i = 0; i < WIDTH-1; i++)
       R[i+1] <= R[i];
    R[0] <= RWire;
    green <= Wire8;
end 
assign DO = green;
assign bluewire = R[7];
endmodule
