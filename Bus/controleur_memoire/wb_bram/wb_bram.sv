//-----------------------------------------------------------------
// Wishbone BlockRAM
//-----------------------------------------------------------------
//
// Le paramètre mem_adr_width doit permettre de déterminer le nombre 
// de mots de la mémoire : (2048 pour mem_adr_width=11)

module wb_bram #(parameter mem_adr_width = 11) (
      // Wishbone interface
            wshb_if.slave wb_s
      );

logic ack_read;
logic ack_write;
logic[7:0] mem1 [0:2**mem_adr_width-1];
logic[7:0] mem2 [0:2**mem_adr_width-1];
logic[7:0] mem3 [0:2**mem_adr_width-1];
logic[7:0] mem4 [0:2**mem_adr_width-1];


logic WriteEnable[3:0]; 
genvar i;
for(i = 0; i < 4; i++) begin
    assign WriteEnable[i] = wb_s.we & wb_s.stb & wb_s.sel[i];
end

wire [mem_adr_width-1:0] addr = wb_s.adr[mem_adr_width+1:2];

always_ff @(posedge wb_s.clk)
begin
   if(WriteEnable[0]) begin
      mem1[addr] <= wb_s.dat_ms[7:0];
   end
   if(WriteEnable[1]) begin
      mem2[addr] <= wb_s.dat_ms[15:8];
   end
   if(WriteEnable[2]) begin
      mem3[addr] <= wb_s.dat_ms[23:16];
   end
   if(WriteEnable[3]) begin
      mem4[addr] <= wb_s.dat_ms[31:24];
   end
   wb_s.dat_sm <= {mem4[addr],mem3[addr],mem2[addr],mem1[addr]};
end




assign ack_write = wb_s.we & wb_s.stb;

always_ff @(posedge wb_s.clk,posedge wb_s.rst) begin
      if(wb_s.rst) begin
            ack_read <= 0;
      end
      else 
      begin
      if(!wb_s.we & wb_s.stb)
            ack_read <= !ack_read;
      end
end
assign wb_s.err = 0;
assign wb_s.rty = 0;
assign wb_s.ack = ack_read | ack_write;

endmodule